import React, { forwardRef } from "react";
import { Box, SimpleGrid, Grid, Text } from "@chakra-ui/react";
import QRCode from "react-qr-code";
import styles from "./style.module.css";

const ComponentToPrint = forwardRef((props, ref) => {
  return (
    <Box display={"block"} ref={ref}>
      <Box
        width="21cm"
        height="28.7cm"
        // borderWidth="1px"
      >
        <Box
          width="19.7cm" // JANGAN DIUBAH
          height="28.4cm" // JANGAN DIUBAH
          // borderWidth="1px"
          marginStart="0.65cm"
          marginEnd="0.7cm"
          marginBottom="0.7cm"
          marginTop="0.7cm"
        >
          <SimpleGrid columns={5}>
            {props.data.map((item, index) => (
              <Box
                key={index}
                width="3.94cm" // JANGAN DIUBAH
                height="2.175cm"
                // borderWidth="1px"
                padding="3px"
              >
                <Box
                  padding="2px"
                  // borderWidth="1px"
                  borderRadius="5px"
                  height="100%"
                  style={{ backgroundColor: "red" }}
                  className={styles.stripe_ca}
                >
                  <Grid templateColumns="repeat(2, 1fr)" marginTop="5px">
                    <Box style={{ textAlign: "center" }} mt="2px">
                      <Box display={"flex"} justifyContent="center">
                        <QRCode
                          size={50}
                          width="100%"
                          value={item.sampleData.code}
                        />
                      </Box>
                      <Text fontSize="6px" fontWeight="bold" mt="2px">
                        {item.sampleData.code}
                      </Text>
                    </Box>
                    <Box>
                      <Text fontSize="6px" fontWeight="bold">
                        Contact Agar
                      </Text>
                      <Text fontSize="6px" fontWeight="bold">
                        {item.sampleData.samplingPointCode} (
                        {item.sampleData.gradeRoomName
                          ? item.sampleData.gradeRoomName
                          : item.requestQc.emRoomGradeName}
                        )
                      </Text>
                      <Text fontSize="6px" fontWeight="bold">
                        {item.requestQc.noBatch}
                      </Text>
                      {item.samplingQc.samplingDateFrom &&
                        item.samplingQc.samplingDateTo && (
                          <Text mt="2px" fontSize="5.5px">
                            16 Des 2022-16 Des 2022
                          </Text>
                        )}
                    </Box>
                  </Grid>
                </Box>
              </Box>
            ))}
          </SimpleGrid>
        </Box>
      </Box>
    </Box>
  );
});

export default ComponentToPrint;
