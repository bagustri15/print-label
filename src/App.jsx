import React, { useEffect, useState, useRef } from "react";
import { Box, Button } from "@chakra-ui/react";
import { useReactToPrint } from "react-to-print";
import ComponentToPrint from "./ComponentToPrint";

import { data } from "./data";

export default function App() {
  const printedComponentRef = useRef();

  const handlePrint = useReactToPrint({
    content: () => printedComponentRef.current,
  });

  return (
    <>
      <Box display="flex" justifyContent="center" m="5" flexDirection="column">
        <Button mb="3" onClick={handlePrint}>
          Print Label
        </Button>
        <Box m="auto">
          <ComponentToPrint ref={printedComponentRef} data={data} />
        </Box>
      </Box>
    </>
  );
}
